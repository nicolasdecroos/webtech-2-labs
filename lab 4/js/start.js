/*global  $, Skycons*/
(function () {
    'use strict';

    var skycons = new Skycons({
        "color": "#FFF"
    });
	
	var date = new Date();
	var daysArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saterday"];
    var monthsArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


	var App = { 

		key: "5b0ccf0bc14cff8443b772c2c0946fb5",
		lat: "",
		long: "",

		init: function () {
		   	App.getLocation();
		},

		getLocation: function () {
	        navigator.geolocation.getCurrentPosition(App.foundPosition);
	    },

	    foundPosition: function (pos) {
	        App.lat = pos.coords.latitude;
	        App.long = pos.coords.longitude;
	        App.getWeather();
	    },

	    getWeather: function () {
	    	var ApiRequest = "https://api.forecast.io/forecast/" + App.key + "/" + App.lat + "," + App.long + "?units=si";

	    	$.ajax({
                url: ApiRequest,
                dataType: "jsonp",
                success: function (data) {
                    App.getToday(data);  
                    App.getTommorow(data);            
				}
			});
	    },

	    getToday: function(data){
		    document.getElementById("day").innerHTML = daysArray[date.getDay()];
            document.getElementById("date").innerHTML = date.getDate() + " " + monthsArray[date.getMonth()];
            document.getElementById("degrees").innerHTML = data.currently.temperature + " °C";
            document.getElementById("weather-summary").innerHTML = data.currently.summary;

            // ICON                  
            if (data.daily.data[0].icon == "rain") 
            {
                skycons.set("weather-icon", Skycons.RAIN);
            } 
            else if (data.daily.data[0].icon == "clear-day") 
            {
                skycons.set("weather-icon", Skycons.CLEAR_DAY);
            } 
            else if (data.daily.data[0].icon == "partly-cloudy-day") 
            {
                skycons.set("weather-icon", Skycons.PARTLY_CLOUDY_DAY);
            } 
            else if (data.daily.data[0].icon == "sleet")
            {
                skycons.set("weather-icon", Skycons.SLEET);
            } 
            else if (data.daily.data[0].icon == "snow") 
            {
                skycons.set("weather-icon", Skycons.SNOW);
            }        
        },

        getTommorow: function(data){
		    document.getElementById("day-tommorow").innerHTML = daysArray[date.getDay() + 1 ];
            document.getElementById("date-tommorow").innerHTML = date.getDate() + 1 + " " + monthsArray[date.getMonth()];
            document.getElementById("degrees-tommorow").innerHTML = data.daily.data[1].temperatureMax + " °C";
            document.getElementById("weather-summary-tommorow").innerHTML = data.daily.data[1].summary;

            // ICON                  
            if (data.daily.data[1].icon == "rain") 
            {
                skycons.set("weather-icon-tommorow", Skycons.RAIN);
            } 
            else if (data.daily.data[1].icon == "clear-day") 
            {
                skycons.set("weather-icon-tommorow", Skycons.CLEAR_DAY);
            } 
            else if (data.daily.data[1].icon == "partly-cloudy-day") 
            {
                skycons.set("weather-icon-tommorow", Skycons.PARTLY_CLOUDY_DAY);
            } 
            else if (data.daily.data[1].icon == "sleet")
            {
                skycons.set("weather-icon-tommorow", Skycons.SLEET);
            } 
            else if (data.daily.data[1].icon == "snow") 
            {
                skycons.set("weather-icon-tommorow", Skycons.SNOW);
            }          
        }

	}
	App.init();
	skycons.play();
}());
