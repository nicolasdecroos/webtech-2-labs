var express = require('express');
var router = express.Router();
var Topic = require('../models/chatModel.js');

    router.get('/', function(req, res, next) {
        res.render('chat.ejs', { title: 'Chat', user: req.user });
    });

module.exports = router;