$(document).ready(function () {

    var socket = io.connect();
    var $msgForm = $('#create_msg_form');
    var $msgDiv = $('#msgDiv');
    var $chatBody = $('#chatBody');
    var $username = $('#username');

    $msgForm.submit(function (e) {
        e.preventDefault();

        socket.emit('Create Message', {
            chatBody: $chatBody.val(), 
            username: $username.val()
        });
        $chatBody.val('');
    });

    socket.on('Get Messages', function (docs) {
        for (var i = 0; i < docs.length; i++) {
            displayMsg(docs[i]);
        }
    });

    socket.on('Get New Messages', function (data) {
        displayMsg(data);
    });

    
    function displayMsg(data) {   
        $msgDiv.prepend("<p>" + data.username + ": " + data.chatBody + "</p>");
    }
});
