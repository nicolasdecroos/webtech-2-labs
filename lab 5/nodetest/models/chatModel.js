var mongoose = require('mongoose');

//Schema
var ChatSchema = mongoose.Schema({
    chatBody   : String,
    username    : String
});

//Model
Chat = mongoose.model('Chat', ChatSchema);
module.exports = Chat;